# Conjoined

This is just a hackish way to connect two machines together over SSH even when
one of those machines is crippled software.


## Features

Basically this lets you pass messages, sort of a poor man's clipboard between
machines.  You paste text into one terminal and it appears on the other
machine.  The target machine will also open any URLs passed along in the
default browser.


## How to use it

The connection must be initiated from one machine to the other first.  If one
of the machines in question is a Windows box, you probably want to start there
since connecting to Windows from Linux over SSH can be fiddly.

You connect from Box A to Box B over SSH while specifying a `RemoteForward` to
`localhost:22`.  If you're trying to disguise the traffic, you should probably
configure the target server (Box B) to listen on an inconspicuous port, like
`443`.  Here's a sample config for the source machine (Box A):

    # ${HOME}/.ssh/config (Box A)
    Host box-b
      Hostname box-a.mynetwork.local
      Port 443
      RemoteForward 8888 localhost:22

Then, on the target machine (Box B) you add Box A's public key to
`${HOME}/.ssh/authorized_keys` with a `command=` argument:

    # ${HOME}/.ssh/authorized_keys (Box B)
    command="/path/to/conjoined-pipeline" ssh-ed25519 AAAAC...bXIh myusername@box-a

Once the above is done, just SSH into `box-b` from Box A:

```shell
box-a $ ssh box-b
```

The connection will not render a Bash prompt, but rather will just look like this:

```shell
box-a $ ssh box-b
> 
```

At this point, anything you enter will be streamed into the pipe on Box B, but
Box B isn't listening to the pipe, so nothing will happen.

Next, we setup the listener by just running `conjoined-listener` on Box B:

```shell
box-b $ conjoined-listener
```

This will appear to just hang and do nothing, until you enter some text into
that `> ` prompt on Box A, at which point it'll magically appear on Box B!

So now we've got one-way communication.  To go back the other way, the process
is much the same.  Thanks to the `RemoteForward` line in Box A's SSH config,
Box B can now SSH into Box A directly:

```shell
box-b $ ssh box-a
```

You can then repeat the same pattern on the other side: add the `command=`
argument to the `authorized_keys` file on Box A and then run
`conjoined-listener` on Box A.  The one caveat of course is that you may want
to specify a particular key when going back the other way, otherwise you'd
effectively disable your ability to shell into Box A from Box B since the
default would always just start `conjoined-pipeline`.
